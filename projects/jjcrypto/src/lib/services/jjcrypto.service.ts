import { Injectable } from "@angular/core";
import { RsaAlgorithm } from "../algorithms/rsa-algorithm";
import { Key } from "../interfaces/key";

@Injectable()
export class JjCryptoService {
    private _rsaAlgorithm: RsaAlgorithm;
    constructor() {
        this._rsaAlgorithm = new RsaAlgorithm();
    }

    public getPublicKey(): Key {
        return this._rsaAlgorithm.getPublicKey();
    }
    public encryptWithKey(m: string, publicKey: Key): string {
        const numbers = this._convertStringToEncrypt(m);
        console.log('the numbers list', numbers);
        let encryptedNumbers: number[] = [];
        numbers.forEach(n => {
            const encrypted = this._rsaAlgorithm.encrypt(n, publicKey);
            encryptedNumbers.push(encrypted);
        })
        console.log('encrypted numbers', encryptedNumbers);
        return this._convertNumbersToString(encryptedNumbers);
    }

    public encrypt(m: string): string {
        return this.encryptWithKey(m, this.getPublicKey());
    }

    public decrypt(c: string): string {
        const numbers = this._convertStringToDecrypt(c);
        console.log('the numbers of decrypt', numbers);
        let res = '';
        numbers.forEach(n => {
            const decrypted = this._rsaAlgorithm.decrypt(n);
            res = res + String.fromCharCode(decrypted);
        })
        return res
    }

    private _convertStringToEncrypt(m: string): number[] {
        let res: number[] = [];
        for (let i = 0; i < m.length; i++) {
            const codeNumber = m.charAt(i).charCodeAt(0);
            res.push(codeNumber);
        }
        return res;
    }

    private _convertStringToDecrypt(c: string): number[] {
        let res: number[] = [];
        for (let i = 0; i < c.length - 1; i += 2) {
            const codeNumber1 = 126 - c.charAt(i).charCodeAt(0);
            const codeNumber2 = 126 - c.charAt(i + 1).charCodeAt(0);
            const realNumber = 94 * codeNumber1 + codeNumber2;
            res.push(realNumber);
        }
        return res;
    }

    private _convertNumbersToString(numbers: number[]): string {
        let res = '';
        numbers.forEach(n => {
            const div = 126 - (Math.floor(n / 94));
            res = res + String.fromCharCode(div);
            const mod = 126 - (n % 94);
            res = res + String.fromCharCode(mod);
        })
        return res;
    }
}