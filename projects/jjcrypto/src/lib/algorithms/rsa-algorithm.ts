import { Key } from '../interfaces/key';
import { Coprime } from '../util/coprime';
import { ModularArithmetic } from '../util/modular-aritmethic';
import { PrimeNumbers } from '../util/prime-numbers';

export class RsaAlgorithm {
    private _primeNumbers: PrimeNumbers;
    private _coprime: Coprime;
    private _privateKey: Key;
    private _publicKey: Key;

    constructor() {
        this._primeNumbers = new PrimeNumbers();
        this._coprime = new Coprime(this._primeNumbers);
        const p = this._primeNumbers.getRandomPrimeNumber();
        //const p= 197;
        console.log('prime number p', p);
        let q = this._primeNumbers.getRandomPrimeNumber();
        //let q = 199;
        while(q === p) {
            q = this._primeNumbers.getRandomPrimeNumber();
        }
        console.log('prime number q', q);
        const n = p * q;
        const phiN = (p - 1) * (q - 1);
        console.log('phi de N', phiN);
        const e = this._coprime.getCoprimeNumber(phiN);
        const d = this._calculateD(e, phiN);
        this._privateKey = {
            x: d,
            n: n
        }
        this._publicKey = {
            x: e,
            n: n
        }
        console.log('the public key', this._publicKey);

        console.log('the private key', this._privateKey);
    }

    public getPublicKey(): Key {
        return this._publicKey;
    }

    public encrypt(m: number, publicKey: Key): number {
        return ModularArithmetic.modOfExponentation(m, publicKey.x, publicKey.n);
    }

    public decrypt(c: number): number {
        return ModularArithmetic.modOfExponentation(c, this._privateKey.x, this._privateKey.n);
    }

    private _calculateD(e: number, phiN: number): number {
        let candidateNumber = phiN + 1;
       while (candidateNumber % e !== 0) {
            candidateNumber += phiN;
        }
        return candidateNumber / e;
    }

    private calcularExp(x: number, y: number, p: number):number {

        // https://es.khanacademy.org/computing/computer-science/cryptography/modarithmetic/a/fast-modular-exponentiation
        let res = 1;     // Initialize result
 
    while (y > 0)
    {
        // If y is odd, multiply x with result
        if (y & 1)
            res = res*x;
 
        // y must be even now
        y = y>>1; // y = y/2
        x = x*x; // Change x to x^2
    }
    return res % p;
    }
}