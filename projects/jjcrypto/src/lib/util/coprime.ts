import { PrimeNumbers } from "./prime-numbers";

export class Coprime {
    private _primeNumbers: PrimeNumbers;
    constructor(primeNumbers: PrimeNumbers) {
        this._primeNumbers = primeNumbers;
    }

    public getCoprimeNumber(n: number): number {
        const primeNumbers = this._primeNumbers.getPrimeNumbers()
        for (let i = 12; i < primeNumbers.length; i++) {
            if (this.isCoprime(i, n)) {
                return i;
            }
        }
        return 0;
    }

    public isCoprime(a: number, b: number): boolean {
        const min = Math.min(a, b);
        let i = 2;
        while (i <= min) {
            if(a%i === 0 && b%i ===0){
                return false;
            }
            i++;
        }
        return true;
    }
}