export class ModularArithmetic {
    public static modOfExponentation(base: number, exp: number, mod: number): number {
        const bits = this.getBitsOfNumber(exp);
        const valuesPowerOfTwo: number[] = [1];
        for (let i = 1; i < bits.length; i++) {
            valuesPowerOfTwo[i] = valuesPowerOfTwo[i - 1] * 2;
        }
        const baseModValue = base % mod;
        const moduleOfPowerTwoNumbers: number[] = [baseModValue];
        for (let i = 1; i < bits.length; i++) {
            const previousMod = moduleOfPowerTwoNumbers[i - 1];
            moduleOfPowerTwoNumbers.push((previousMod * previousMod) % mod);
        }
        let productOfModules = BigInt(1);
        for (let i = 0; i < bits.length; i++) {
            if (bits[i]) {
                productOfModules *= BigInt(moduleOfPowerTwoNumbers[i]);
            }
        }
        const res = productOfModules % BigInt(mod);
        return Number(res);
    }

    private static getBitsOfNumber(n: number): boolean[] {
        let res: boolean[] = [];
        while (n > 0) {
            res.push(n % 2 === 1);
            n = Math.floor(n / 2);
        }
        return res;
    }
}