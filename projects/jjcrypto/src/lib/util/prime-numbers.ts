export class PrimeNumbers {
    private _primeNumbers: number[];
    private _numbers: boolean[];
    constructor() {
        this._primeNumbers = [];
        this._numbers = new Array(200);
        this._findPrimeNumbers();
    }

    public getPrimeNumbers(): number[] {
        return this._primeNumbers;
    }

    public getRandomPrimeNumber(): number {
        const random = Math.floor(Math.random() * 19)+6;
        console.log('the random number', random);
        return this._primeNumbers[random];
    }

    private _findPrimeNumbers(): void {
        this._numbers[0] = true;
        for (let i = 1; i < this._numbers.length; i++) {
            if (!this._numbers[i]) {
                this._primeNumbers.push(i + 1);
                this._markNoPrimeNumber(i + 1, i);
            }
        }
    }

    private _markNoPrimeNumber(currentPrime: number, numberPosition: number): void {
        numberPosition = numberPosition + currentPrime;
        while (numberPosition < this._numbers.length) {
            this._numbers[numberPosition] = true;
            numberPosition = numberPosition + currentPrime;
        }
    }
}