import { ModuleWithProviders } from '@angular/core';
import { NgModule } from '@angular/core';
import { JjCryptoService } from './services/jjcrypto.service';

@NgModule({
  declarations: [
  ],
  imports: [
  ],
  exports: [
  ]
})
export class JjcryptoModule {
  static forRoot(): ModuleWithProviders<JjcryptoModule> {
    return {
      ngModule: JjcryptoModule,
      providers: [
        JjCryptoService
      ]
    }
  }
 }
