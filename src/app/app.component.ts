import { Component } from '@angular/core';
import { JjCryptoService } from 'projects/jjcrypto/src/lib/services/jjcrypto.service';
import { ModularArithmetic } from 'projects/jjcrypto/src/lib/util/modular-aritmethic';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'jjcrypto-lib';
  textToEncrypted: string;
  textToDecrypted: string;
  resultEncrypted: string;
  resultDecrypted: string;

  constructor(private _jjCryptoService: JjCryptoService) {
    const encrypted = this._jjCryptoService.encrypt('texto Prueba');
    console.log('the number encrypted', encrypted);
    const decrypted = this._jjCryptoService.decrypt(encrypted);
    console.log('the number decrypted', decrypted);
    this.textToEncrypted = '';
    this.textToDecrypted = '';
    this.resultDecrypted = '';
    this.resultEncrypted = '';
  }

  decryptClick() {
    console.log('text decrypted', this.textToDecrypted);
    this.resultDecrypted = this._jjCryptoService.decrypt(this.textToDecrypted);
  }

  encryptClick() {
    console.log('text decrypted', this.textToEncrypted);
    this.resultEncrypted = this._jjCryptoService.encrypt(this.textToEncrypted);
  }
}
