import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { JjCryptoService } from 'projects/jjcrypto/src/lib/services/jjcrypto.service';

import { AppComponent } from './app.component';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    FormsModule
  ],
  providers: [JjCryptoService],
  bootstrap: [AppComponent]
})
export class AppModule { }
